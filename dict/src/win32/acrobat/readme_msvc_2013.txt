You need Microsoft Visual Studio 2013 Update 2 to compile the Acrobat plug-in.
The Acrobat SDK is needed too.


You can use sdk91_v2_win.zip
Exctract "Adobe/Acrobat 9 SDK/Version 1/PluginSupport/Headers" to "Acrobat_Headers" into this directory.


or sdk110_v1_win.zip
Exctract "Adobe/Acrobat XI SDK/Version 1/PluginSupport/Headers" to "Acrobat_Headers" into this directory.
For "Acrobat_Headers\API\PgCntProcs.h", you need to add a UTF-8 BOM in its head to fix the compile problem. Just use the notepad to open it then save.

In Visual Studio 2013, you need to select "Release" but not "Debug"!

After compile success, you will find "dict/msvc_2013/Release/acrobat-wordpick-plugin.dll" file! You need to rename it to "Release/StarDict.api"!


This plug-in only works on Abode Acrobat Professional currently. For Adobe Reader, a license is needed, which will cost $2,500, this has not be done yet.
